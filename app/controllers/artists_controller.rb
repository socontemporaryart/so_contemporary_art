class ArtistsController < ApplicationController
  expose_decorated(:artists)
  expose_decorated(:artist, attributes: :artist_params)

  def index
  end

  def show
    render :show, layout: 'artist'
  end

  def new
  end

  def create
    if artist.save
      redirect_to artists_path
    else
      render :new
    end
  end

  def edit
  end

  def update
  end

  def destroy
  end

  private

  def artist_params
    params.require(:artist).permit(:first_name, :last_name, :description)
  end
end
