class ArtPiecesController < ApplicationController
  expose_decorated(:art_piece)
  expose_decorated(:art_pieces)

  def index
    return if params[:artist].blank?
    self.art_pieces = ArtPiece.by_artist(params[:artist])
  end

  def show

  end

  private

  def filter_params
    params.require(:filter).permit(:artist)
  end

  def art_piece_params
  end
end
