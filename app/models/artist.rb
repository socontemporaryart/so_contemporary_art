class Artist < ActiveRecord::Base
  has_many :art_pieces, inverse_of: :artist

  def to_s
    name
  end
end
