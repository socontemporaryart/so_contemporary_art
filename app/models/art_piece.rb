class ArtPiece < ActiveRecord::Base

  belongs_to :artist

  has_attached_file :image, default_url: "/images/:style/missing.png",
    styles: { medium: "300x300>" }

  validates_attachment :image, presence: true, content_type: { content_type: /\Aimage\/.*\Z/ }

  scope :highlighted, -> { where(highlighted: true) }
  scope :by_artist, -> (name) { includes(:artist).where(artists: { name: name })}

  def author
    artist
  end
end
