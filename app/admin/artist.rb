ActiveAdmin.register Artist do
  includes :art_pieces
  permit_params :name, :description

  form do |f|
    inputs 'Details' do
      input :name
      input :description, as: :text
    end
    f.actions
  end

  sidebar "Artist Details", only: [:show, :edit] do
    ul do
      li link_to "Art Pieces",
        admin_artist_art_pieces_path(artist)
    end
  end

end
