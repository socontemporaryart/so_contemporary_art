ActiveAdmin.register ArtPiece do
  belongs_to :artist, optional: true
  permit_params :name, :description, :artist_id, :image, :highlighted, :price

  form do |f|
    inputs 'Details' do
      input :name
      input :description, as: :text
      input :price, input_html: { value: number_with_precision(object.price, precision: 2), step: '0.01' }
      input :artist, collection: Artist.all
      input :image, required: true, hint: image_tag(f.object.image.url(:medium))
      input :highlighted
    end
    f.actions
  end
end
