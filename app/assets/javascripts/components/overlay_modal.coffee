$(document).ready ->
  SO = SO || {}

  SO.overlayModal =
    init: ->
      $(".close-overlay-modal-button").click(@closeOverlay)

    closeOverlay: ->
      modalSelector = "." + $(this).data("modal-class")
      $('html, body').removeClass('overlayed');
      $(modalSelector).removeClass("open")

