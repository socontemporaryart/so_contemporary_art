$(document).ready ->
  SO = SO || {}

  SO.artPiece =
    init: ->
      @components.makeOfferButton.click(@openMakeOfferModal)
      @components.form.submit(@submitForm)

    components:
      makeOfferButton: $(".art-piece-buttons__make-offer")
      makeOfferModal: $(".art-piece-make-an-offer-modal")
      email:
        label: $(".art-piece-make-an-offer-modal__email label")
        input: $(".art-piece-make-an-offer-modal__email input")
      message:
        label: $(".art-piece-make-an-offer-modal__message label")
        input: $(".art-piece-make-an-offer-modal__message textarea")
      form: $(".art-piece-make-an-offer-modal__form")


    openMakeOfferModal: (e) ->
      e.preventDefault()
      $('body').toggleClass('overlayed');
      SO.artPiece.components.makeOfferModal.toggleClass("open")

    submitForm: (e) =>
      e.preventDefault()
      valid = SO.artPiece.isEmailValid() && SO.artPiece.isMessageValid()


    isEmailValid: ->
      @components.email.input.val().length > 0

    isMessageValid: ->
      @components.message.input.val().length > 0

  SO.artPiece.init()

