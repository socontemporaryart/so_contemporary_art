module ArtPiecesHelper
  def gallery_filtered_by_artist_path(artist)
    gallery_path + "?artist=" + artist.name.gsub(/\s/, '+')
  end

  def price_in_pounds(price)
    number_to_currency(price, unit: '£')
  end
end
