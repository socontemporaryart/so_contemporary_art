class ArtPieceDecorator < Draper::Decorator
  delegate_all

  def image_aspect_ratio
    image.width.to_f / image.height
  end

  def wide_or_tall?
    image_aspect_ratio > 1.0 ? :wide : :tall
  end

  def wide?
    wide_or_tall? == :wide
  end

  def tall?
    wide_or_tall? == :tall
  end

  def description_with_line_breaks
    description.gsub("\r\n", '<br/>')
  end
end
