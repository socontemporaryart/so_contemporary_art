class RemoveFirstNameAndLastNameFromArtists < ActiveRecord::Migration
  def change
    remove_column :artists, :first_name
    remove_column :artists, :last_name
  end
end
