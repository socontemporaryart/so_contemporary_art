class CreateArtPiece < ActiveRecord::Migration
  def change
    create_table :art_pieces do |t|
      t.integer :artist_id
      t.string :name
      t.text :description
      t.timestamps
    end
  end
end
