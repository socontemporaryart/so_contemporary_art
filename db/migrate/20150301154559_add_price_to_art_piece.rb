class AddPriceToArtPiece < ActiveRecord::Migration
  def change
    add_column :art_pieces, :price, :decimal, precision: 15, scale: 2, null: false, default: 0.0
  end
end
