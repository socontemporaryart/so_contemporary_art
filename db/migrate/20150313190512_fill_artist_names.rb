class FillArtistNames < ActiveRecord::Migration
  def up
    Artist.all.each { |a| a.update_attributes(name: a.to_s) }
  end

  def down
    puts "Can't reverse this"
  end
end
