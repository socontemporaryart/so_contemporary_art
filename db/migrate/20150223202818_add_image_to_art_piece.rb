class AddImageToArtPiece < ActiveRecord::Migration
  def up
    add_attachment :art_pieces, :image
  end

  def down
    remove_attachment :art_pieces, :image
  end
end
