class AddHighlightedToArtPiece < ActiveRecord::Migration
  def change
    add_column :art_pieces, :highlighted, :boolean, default: false
  end
end
