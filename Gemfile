source 'https://rubygems.org'
source 'https://rails-assets.org'

gem 'activeadmin', github: 'activeadmin'
gem 'active_model_serializers', '~> 0.8.3'
gem 'activerecord_any_of'
gem 'aws-sdk', '< 2.0'
gem 'bcrypt', '~> 3.1.7'
gem 'decent_decoration'
gem 'decent_exposure'
gem 'dependor'
gem 'devise'
gem 'draper'
gem 'friendly_id'
gem 'gon'
gem 'jquery-rails'
gem 'mail_form'
gem 'meta-tags'
gem 'paperclip'
gem 'paperclip-meta'
gem 'pg'
gem 'rails', '4.2.0'
gem 'reform'
gem 'searchlight'
gem 'simple_form'
gem 'slim'

gem 'bourbon'
gem 'compass-rails'
gem 'modernizr-rails'
gem 'neat'
gem 'sass-rails'

group :assets do
  gem 'coffee-rails'
  # gem 'paloma', '~> 4.1.1'
  gem 'refills'
  gem 'uglifier', '>= 1.3.0'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  # gem 'guard-rspec', require: false
  # gem 'guard-rubocop'
  gem 'meta_request'
  # gem 'rack-mini-profiler'
  gem 'thin'
end

group :test do
  # gem 'codeclimate-test-reporter', require: nil
  gem 'shoulda-matchers', require: false
  gem 'orderly'
end

group :development, :test do
  gem 'awesome_print'
  gem 'bullet'
  gem 'bogus', require: false
  gem 'capybara'
  gem 'database_cleaner'
  gem 'dotenv-rails'
  gem 'factory_girl_rails'
  gem 'ffaker'
  gem 'jasmine-rails'
  gem 'jazz_hands', github: 'netguru/jazz_hands'
  gem 'letter_opener'
  gem 'poltergeist', require: false
  gem 'quiet_assets'
  gem 'rspec-rails'
  gem 'spring'
  # gem 'spring-commands-rspec'
  gem 'web-console', '~> 2.0'
end

group :production do
  gem 'passenger'
  gem 'rails_12factor'
end
